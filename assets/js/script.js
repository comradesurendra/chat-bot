$(document).ready(function() {

	// Credentials
	var baseUrl = "https://aws-prescribe-test-server.herokuapp.com/chat-bot";

	//---------------------------------- Add dynamic html bot content(Widget style) ----------------------------
	// You can also add the html content in html page and still it will work!
	var mybot = '<div class="chatCont" id="chatCont">'+
								'<div class="bot_profile">'+
									'<img src="assets/img/nursing-nurse.png" class="bot_p_img">'+
									'<div class="close">'+
										'<i class="fa fa-times" aria-hidden="true"></i>'+
									'</div>'+
								'</div><!--bot_profile end-->'+
								'<div id="result_div" class="resultDiv"></div>'+
								'<div class="chatForm" id="chat-div">'+
									'<div class="spinner">'+
										'<div class="bounce1"></div>'+
										'<div class="bounce2"></div>'+
										'<div class="bounce3"></div>'+
									'</div>'+
									'<input type="text" id="chat-input" autocomplete="off" placeholder="Start typing here"'+ 'class="form-control bot-txt"/>'+
								'</div>'+
							'</div><!--chatCont end-->'+

							'<div class="profile_div">'+
								'<div class="row">'+
									'<div class="col-hgt">'+
										'<img src="assets/img/circle.png" class="img-circle img-profile">'+
									'</div><!--col-hgt end-->'+
									'<div class="col-hgt">'+
										'<div class="chat-txt">'+
											'Chat with us now!'+
										'</div>'+
									'</div><!--col-hgt end-->'+
								'</div><!--row end-->'+
							'</div><!--profile_div end-->';

	$("mybot").html(mybot);
	// ------------------------------------------ Toggle chatbot -----------------------------------------------
	$('.profile_div').click(function() {
		$('.profile_div').toggle();
		$('.chatCont').toggle();
		$('.bot_profile').toggle();
		$('.chatForm').toggle();
		document.getElementById('chat-input').focus();
	});

	$('.close').click(function() {
		$('.profile_div').toggle();
		$('.chatCont').toggle();
		$('.bot_profile').toggle();
		$('.chatForm').toggle();
	});


	// Session Init (is important so that each user interaction is unique)--------------------------------------
	var session = function() {
		// Retrieve the object from storage
		if(sessionStorage.getItem('session')) {
			var retrievedSession = sessionStorage.getItem('session');
		} else {
			// Random Number Generator
			var randomNo = Math.floor((Math.random() * 1000) + 1);
			// get Timestamp
			var timestamp = Date.now();
			// get Day
			var date = new Date();
			var weekday = new Array(7);
			weekday[0] = "Sunday";
			weekday[1] = "Monday";
			weekday[2] = "Tuesday";
			weekday[3] = "Wednesday";
			weekday[4] = "Thursday";
			weekday[5] = "Friday";
			weekday[6] = "Saturday";
			var day = weekday[date.getDay()];
			// Join random number+day+timestamp
			var session_id = randomNo+day+timestamp;
			// Put the object into storage
			sessionStorage.setItem('session', session_id);
			var retrievedSession = sessionStorage.getItem('session');
		}
		return retrievedSession;
		// console.log('session: ', retrievedSession);
	}

	// Call Session init
	var mysession = session();

	// on input/text enter--------------------------------------------------------------------------------------
	$('#chat-input').on('keyup keypress', function(e) {
		var keyCode = e.keyCode || e.which;
		var text = $("#chat-input").val();
		if (keyCode === 13) {
			if(text == "" ||  $.trim(text) == '') {
				e.preventDefault();
				return false;
			} else {
				// $("#chat-input").blur();
				setUserResponse(text);
				send(text);
				e.preventDefault();
				return false;
			}
		}
	});


	//------------------------------------------- Send request to API.AI ---------------------------------------
	async function send(text) {
		data = {
			message: text
		}
		await fetch(baseUrl, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify(data),
				})
				.then(response => response.json())
				.then(res => {
				main(res)
				})
				.catch((error) => {
				console.error('Error:', error);
				});
	}


	//------------------------------------------- Main function ------------------------------------------------
	function main(data) {
		const result = data.data
		// use incomplete if u use required in api.ai questions in intent
		// check if actionIncomplete = false
		if(result) { // check if messages are there
			if(result > 0) { //check if quick replies are there
				var suggestions = result[1];
			}
		}
		switch(result) {
			// case 'your.action': // set in api.ai
			// Perform operation/json api call based on action
			// Also check if (incomplete = false) if there are many required parameters in an intent
			// if(suggestions) { // check if quick replies are there in api.ai
			//   addSuggestion(suggestions);
			// }
			// break;
			default:
				setBotResponse(result);
				if(suggestions) { // check if quick replies are there in api.ai
					addSuggestion(suggestions);
				}
				break;
		}
	}

	// function myFunction(e){
	// 	// const val = document.getElementById(value);
	// 	console.log(e.target.val);
	// }

	function buttonResponse(value) {
		function myFunction(e){
			setUserResponse(e.target.value);
			send(e.target.value)
		}
		if(!value.includes('▪️')){
			let end = value.split('\n\n')
			let res = end[1] ? (`<p class="botResult">${end[0]}</p><br />` + `<p class="botResult">${end[1]}</p><br />` + `<div class="clearfix"></div>`) : (`<p class="botResult">${end[0]}</p><br />` + `<div class="clearfix"></div>`)
			console.log(res)
			$(res).appendTo('#result_div')
		} else {
			let message =value.split('▪️');
			let len = message.length;
			let end = message[len-1].split('\n\n')
			let res = end[1]?(`<p class="botResult">${message[0]}</p><br />` + `<p class="botResult">${end[1]}</p><br />` + `<div class="clearfix"></div>`):(`<p class="botResult">${message[0]}</p><br />` + `<div class="clearfix"></div>`);
			var span = document.createElement('span');
			for(let i = 1;i<len-1;i++) {
				var button = document.createElement('button')
				var text = document.createTextNode(message[i])
				button.appendChild(text)
				button.value = message[i]
				button.className = 'btn_sty'
				button.onclick = myFunction;
				span.appendChild(button)
			}
				var button = document.createElement('button')
				var text = document.createTextNode(end[0])
				button.appendChild(text)
				button.value = end[0]
				button.className = 'btn_sty'
				button.onclick = myFunction;
				span.appendChild(button)
			$(res).appendTo('#result_div')
			$(span).appendTo('#result_div');
			// value = value.replace(new RegExp('\r?\n','g'), '<br />');
			
		}
		
	}

	function urlify(text) {
		var urlRegex = /(https?:\/\/[^\s]+)/g;
		return text.replace(urlRegex, function(url) {
		  return '<a href="' + url + '">' + '<button class="link-button">'+ url + '</button>' +'</a>';
		})
		// or alternatively
		// return text.replace(urlRegex, '<a href="$1">$1</a>')
	  }
	//------------------------------------ Set bot response in result_div -------------------------------------
	function setBotResponse(val) {
		setTimeout(function(){
			if($.trim(val) == '') {
				val = 'I couldn\'t get that. Let\' try something else!'
				var BotResponse = '<p class="botResult">'+val+'</p><div class="clearfix"></div>';
				$(BotResponse).appendTo('#result_div');
			} else {
				val = urlify(val);
				buttonResponse(val)
				// var BotResponse = '<p class="botResult">'+val+'</p><div class="clearfix"></div>';
				// $(BotResponse).appendTo('#result_div');
			}
			scrollToBottomOfResults();
			hideSpinner();
		}, 500);
	}


	//------------------------------------- Set user response in result_div ------------------------------------
	function setUserResponse(val) {
		var UserResponse = '<p class="userEnteredText">'+val+'</p><div class="clearfix"></div>';
		$(UserResponse).appendTo('#result_div');
		$("#chat-input").val('');
		scrollToBottomOfResults();
		showSpinner();
		$('.suggestion').remove();
	}


	//---------------------------------- Scroll to the bottom of the results div -------------------------------
	function scrollToBottomOfResults() {
		var terminalResultsDiv = document.getElementById('result_div');
		terminalResultsDiv.scrollTop = terminalResultsDiv.scrollHeight;
	}


	//---------------------------------------- Ascii Spinner ---------------------------------------------------
	function showSpinner() {
		$('.spinner').show();
	}
	function hideSpinner() {
		$('.spinner').hide();
	}


	//------------------------------------------- Suggestions --------------------------------------------------
	function addSuggestion(textToAdd) {
		setTimeout(function() {
			var suggestions = textToAdd.replies;
			var suggLength = textToAdd.replies.length;
			$('<p class="suggestion"></p>').appendTo('#result_div');
			$('<div class="sugg-title">Suggestions: </div>').appendTo('.suggestion');
			// Loop through suggestions
			for(i=0;i<suggLength;i++) {
				$('<span class="sugg-options">'+suggestions[i]+'</span>').appendTo('.suggestion');
			}
			scrollToBottomOfResults();
		}, 1000);
	}

	// on click of suggestions get value and send to API.AI
	$(document).on("click", ".suggestion span", function() {
		var text = this.innerText;
		setUserResponse(text);
		send(text);
		$('.suggestion').remove();
	});

	// $(function() {
	// 	$('.profile_div').effect( "shake" ;
	//   });

	//   $(function() {
	// 	$( ".chat-txt" ).animate({ 
	// 		opacity:1;
	// 	}, 2000 );
	//   })
	   
	  // Stop animation when button is clicked
	//   $(function() {
	// 	$( ".profile_div" ).stop();
	//   })
	   
	//   // Start animation in the opposite direction
	//   $(function() {
	// 	$( ".profile_div" ).animate({ 
	// 		left: "-=10px",
	// 		right: "+=10px"
	// 	 }, 2000 );
	//   })

	// if (performance.navigation.type == performance.navigation.TYPE_RELOAD) {
	// 	console.info( "This page is reloaded" );
	//   } else {
	// 	console.info( "This page is not reloaded");
	//   }
	
	// $(window).on('hashchange', function () {
	// 	if (hash != window.location.hash) {
	// 		 alert('hash change called');
	// 		 $( "mybot").effect( "shake" );
	// 	}
	// 	hash = window.location.hash;
	// });
	// Suggestions end -----------------------------------------------------------------------------------------
});
